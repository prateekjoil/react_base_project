import React from 'react';
import {Link,Redirect} from 'react-router-dom';
import {dataService} from '../../service/data.service'
import './login.scss';



const initialState = {
  emailId : "",
  password: "",
  emailIdError : "",
  passwordError : "",
  redirect: false
}

export default class Login extends React.Component {
  state=initialState;

  handleChange = event =>{
    const isCheckbox = event.target.type === "checkbox";
    this.setState({
      [event.target.name]:isCheckbox ? event.target.checked : event.target.value
    });
  }
  

  validate = () =>{
    let emailIdError ="";
    let passwordError =""

    if (this.state.password==""){
      passwordError = 'Please Enter Password';
    }
    if (!this.state.emailId.includes('@')){
      emailIdError = 'Envalid emailId';
    }
    if(emailIdError || passwordError)
    {
      this.setState({ emailIdError,passwordError });
      return false;
    }
    return true;
  };

  loginSubmit = event =>{
    event.preventDefault();
    const isValid = this.validate();
    if(isValid){
      delete this.state.emailIdError;
      delete this.state.passwordError;
      delete this.state.redirect;
      dataService('/user-login',this.state).then((result)=>{
        let responseJSON = result;
       if(result.statusCode==200){
        localStorage.setItem('X-Auth-Token', result['jwtToken']);
        this.setState({redirect:true});
        console.log(responseJSON)
       }
        
      })
    this.setState(initialState)
  }
  };

  render() {   
    
    if(this.state.redirect || localStorage.getItem("X-Auth-Token"))
    {
      return (<Redirect to={'/dashboard'} />)
    }
    
    return (
        

<div className="login-page">
<div className="account-page">
  <div className="account-box">
    <div className="bg-img">
      <div className="kotak-logo"></div>
    </div>


    <div className="account-wrapper">
      <div className="container col-lg-7 ">
        <div className="login-logo">
          <h1 className="ssp-bold-class fs-27 color-theme-black">Sign In</h1>
         {/*  <p className="ssp-reg-class fs-14">Welcome back! Please login to your account.</p> */}
        </div>
        <form className="form-signin" onSubmit={this.loginSubmit} >
          <div className="form-group">
            <input type="text" className="form-control" name="emailId" value={this.state.emailId} onChange={this.handleChange}  placeholder="emailId" />
            <span className="validateError">{this.state.emailIdError}</span>
          </div>
          <div className="form-group">
            <input type="password" name="password" value={this.state.password} onChange={this.handleChange}    className="form-control" 
              placeholder="Password"  />
           <span className="validateError">{this.state.passwordError}</span>
          </div>
          <div className="form-group">
          <input type="checkbox" formControlName="rememberMe" />
          <label class="ssp-reg-class fs-16 color-theme-black ml-2" >Remember me</label>
          {/*   <mat-checkbox formControlName="rememberMe">
              <label class="ssp-reg-class fs-16 color-theme-black" >Remember me</label>
            </mat-checkbox> */}
           
            <label className="ssp-reg-class fs-16 color-theme-black float-right forgot-pass">Forgot Password</label>
          </div>
          <div className="form-group d-flex justify-content-center mt-5">
            <button type="submit"
              className="btn btn-primary btn-block account-btn ssp-reg-class fs-14 color-white bg-theme-red ml-0">
              Sign in
            </button>
         {/*   <Link to="/signup" className="btn btn-primary btn-block account-btn ssp-reg-class fs-14 color-theme-black mr-0">
              Sign Up
            </Link> */}
          </div>
        </form>
      </div>
      <label className="tnc ssp-reg-class fs-10 color-theme-black">Term of use. Privacy policy</label>
    </div>
  </div>
</div>
</div>








    );
  }
}