import React from 'react';
import './dashboard.scss'
  import { Link } from 'react-router-dom';
  import SampleTable from '../dashboard/tables/sample-table/sample-table'
  

export default class Dashboard extends React.Component {
  render() {    
    return (  
        
<div className="dashboard">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

<div className="d-flex"></div>


    <div className="d-flex row width-96">
        <div className="col-xs-12 col-md-12 col-sm-12 col-lg-4 graphPadding">
            <div className="card-body shadow-sm bg-white d-flex">
                <div className="chartData col-5">
                    <div className="chartLabel col-12">Total Views</div>
                    <div className="chartAmount col-12 mt-5 ">246K</div>
                    <div className="chartScale col-12 d-flex">
                        <i className="material-icons downArrow">arrow_downward</i>13.8%
                    </div>
                </div>
                <div className=" col-7 d-flex barChart">
                  
                </div>
            </div>
        </div>
        <div className="col-xs-12 col-md-12 col-sm-12 col-lg-4 graphPadding">
            <div className="card-body shadow-sm bg-white d-flex ">
                <div className="chartData col-5">
                    <div className="chartLabel col-12">Total Views</div>
                    <div className="chartAmount col-12 mt-5 ">2453</div>
                    <div className="chartScaleGreen col-12 d-flex">
                        <i className="material-icons upArrow">arrow_upward</i>13.8%
                    </div>
                </div>
                <div className=" col-7 d-flex barChart">
                   
                </div>
            </div>
        </div>
        <div className="col-xs-12 col-md-12 col-sm-12 col-lg-4 graphPadding">
            <div className="card-body shadow-sm bg-white d-flex ">
                <div className="chartData col-5">
                    <div className="chartLabel col-12">Total Views</div>
                    <div className="chartAmount col-12 mt-5 ">$39K</div>
                    <div className="chartScale col-12 d-flex">
                        <i className="material-icons downArrow">arrow_downward</i>13.8%
                    </div>
                </div>
                <div className=" col-7 d-flex barChart">
                    
                </div>
            </div>
        </div>


    </div>


<div className="d-flex row width-table p-0">
<div className="col-12 mt-3 formWidth p-0"><SampleTable /></div>
</div>



    </div>
    );
  }
}