import React from 'react';
import './sample-table.scss'
  import { Link } from 'react-router-dom';
  import {getDataService} from '../../../../service/data.service'

export default class SampleTable extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      patent : []
    }
  }

  componentDidMount(){
    this.getPaitent()
  }

  getPaitent = () =>{
    getDataService('/patient-details-find-all').then((result)=>{
      let responseJSON = result;
      this.setState({patent: responseJSON.patientDetailsList});
      console.log(this.state.patent)
    })
  }
  render() {    
    return (  
   <div className="sampleTable">


<div className="container">
  <div className="row">
    <div className="col-md-12">
      <h6 className="ssp-reg-class fs-27 ">Data table</h6>
      <table className="table table-bordered table-striped  table-hover rt" id="rt1">
        <thead>
          <tr className="tableRow ssp-bold-class">
            <th scope="col" className="color-blue">NAME</th>
            <th scope="col" className="color-blue">GENDER</th>
            <th scope="col" className="color-blue">CONTACT NUMBER</th>
            <th scope="col" className="color-blue">BIRTH DATE</th>
          </tr>
          <tr className="thead-row-2 bg-white{">
            <th scope="col">
              <input type="text" className="border-none" placeholder="Search..." />
            </th>
            <th scope="col">
              <input type="text" className="border-none" placeholder="Search..." />
            </th>
            <th scope="col">
              <input type="text" className="border-none" placeholder="Search..." />
            </th>
            <th scope="col">
              <input type="text" className="border-none" placeholder="Search..." />
            </th>
          </tr>
        </thead>
        <tbody className="fs-16">
       
         {/*  <tr className="ssp-reg-class">
            <td className="color-black ssp-bold-class word-break-keep">
              <span>
                Contract Management System
              </span>
              <div className="dropdown float-right my-auto">
                <i className="material-icons
                 triple-dot mr-1
                 color-light-orange
                 float-right" data-toggle="dropdown">
                  more_vert
                </i>
                <div className="dropdown-menu dropdown-menu-sm-right dropdown-menu-left">
                  <a  className="dropdown-item float-left text-sm">
                    Edit
                  </a>
                  <a  className="dropdown-item float-left text-sm">
                    Delete
                  </a>
                  <a  className="dropdown-item float-left text-sm">
                    Copy
                  </a>
                  <a   className="dropdown-item float-left text-sm">
                    Status
                  </a>
                  <a   className="dropdown-item float-left text-sm">
                    Download file
                  </a>
                </div>
              </div>
            </td>
            <td className="border-data-none">1</td>
            <td className="border-data-none">Software Licence</td>
            <td className="border-data-none">Vendor</td>
            <td className="border-data-none">9,60,000</td>
            <td className="border-data-none">1 Month</td>
            <td className="border-data-none">Active</td>
          </tr> */}


         { this.state.patent.map((patent =>

           <tr>
            <td className="border-data-none">{patent.name}</td>
         <td className="border-data-none">{patent.sex}</td>
            <td className="border-data-none">{patent.phone}</td>
            <td className="border-data-none">{(patent.birthDate)}</td>
          </tr>
           
            
         ))}

        </tbody>
      </table>
    </div>
  </div>

</div>


   </div>
    );
  }
}